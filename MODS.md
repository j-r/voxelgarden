Voxelgarden mods
----------------

This document lists out all mods included in Voxelgarden with their licenses,
links to upstream repo(-s) and descriptions (if necessary) for easier browsing
and maintainability. For more details, please refer to their specific files.

* `bedrock2`
    * Adds an indestructible bedrock layer at the bottom of the world
    * **Upstream:** <https://codeberg.org/Wuzzy/minetest_bedrock2>
    * **License:** MIT
* `beds`
    * Adds beds which optionally allow players to skip night and respawn where
      they slept
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/beds>
    * **License:** LGPLv2.1 or later
* `binoculars`
    * Adds binoculars which allow players to use the <kbd>Zoom</kbd> key
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/binoculars>
    * **License:** MIT
* `boats`
    * Adds driveable boats made out of wood
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/boats>
    * **License:** MIT
* `bones`
    * Adds bones which optionally contain players' items when they die
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/bones>
    * **License:** MIT
* `bucket`
    * Adds buckets made out of tin that can be used to collect, contain and pour
      liquid
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/bucket>
    * **License:** LGPLv2.1 or later
* `carts`
    * Adds driveable carts and rails on which they ride
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/carts>
    * **License:** MIT
* `conifer`
    * Adds conifer trees and wood
    * **License:** LGPLv2.1 or later
* `controls`
    * Utility library for control press/hold/release events
    * **Upstream:** <https://github.com/mt-mods/controls>
    * **License:** MIT
* `creative`
    * Creative mode functionality and `sfinv`-based inventory
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/creative>
    * **License:** MIT
* `default`
    * The core game mod responsible for basic nodes, items, tools, trees,
      chests, furnaces, sounds and more
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/default>
    * **License:** LGPLv2.1 or later
* `doors`
    * Adds modular doors, as well as fence gates
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/doors>
    * **License:** LGPLv2.1 or later
* `dungeon_loot`
    * Populates engine-generated dungeons with chests containing biome-dependent
      loot
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/dungeon_loot>
    * **License:** MIT
* `dye`
    * Adds colorful dyes as both usable items and crafting material
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/dye>
    * **License:** LGPLv2.1 or later
* `env_sounds`
    * Adds environmental sounds to some nodes, constant and on player contact
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/env_sounds>
    * **License:** MIT
* `farming`
    * Adds growable plants and hoes to cultivate soil
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/farming>
    * **License:** MIT
* `fire`
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/fire>
    * **License:** LGPLv2.1 or later
* `fireflies`
    * Adds colorful node-based fireflies and bug net item to catch them and put
      into bottles as decorations
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/fireflies>
    * **License:** MIT
* `flowers`
    * Adds flowers to obtain dyes from and seedlings for flower farming
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/flowers>
    * **License:** LGPLv2.1 or later
* `footsteps`
    * Makes players randomly leave footsteps on the ground
    * **License:** LGPLv2.1 or later
* `game_commands`
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/game_commands>
    * **License:** MIT
* `headanim`
    * Makes player heads follow their looking direction
    * **Upstream:** <https://github.com/LoneWolfHT/headanim>
    * **License:** MIT
* `mobs`
    * **License:** MIT
* `mobs_flat`
    * **License:** LGPLv2.1 or later
* `map`
    * Adds a mapping kit item which allows players to use the minimap
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/map>
    * **License:** MIT
* `mtg_craftguide`
    * Adds a crafting guide in the form of an interactive recipe/usage list
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/mtg_craftguide>
    * **License:** MIT
* `nether`
    * Adds a deep hellish underground realm and supporting content
    * **Upstream:** <https://github.com/minetest-mods/nether>
    * **License:** ISC
* `physics`
    * Replacement for the `builtin` falling node and liquid physics
    * **License:** LGPLv2.1 or later
* `placeitem`
    * Makes most items placeable as nodes
    * **License:** LGPLv2.1 or later
* `player_api`
    * Player-related utilities and API to apply new models and textures
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/player_api>
    * **License:** LGPLv2.1 or later
* `playerphysics`
    * API allowing for independent changes to player physics using modifiers
    * **Upstream:** <https://codeberg.org/Wuzzy/minetest_playerphysics>
    * **License:** MIT
* `screwdriver`
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/screwdriver>
    * **License:** LGPLv2.1 or later
* `sethome`
    * Adds the ability to use `/sethome` and `/home` to teleport between various
      points in the game world
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/sethome>
    * **License:** MIT
* `sfinv`
    * Clean, simple and fast inventory solution using tabs
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/sfinv>
    * **License:** MIT
* `show_wielded_item`
    * Displays the name of the wielded item above the hotbar and statbars
    * **Upstream:** <https://codeberg.org/Wuzzy/minetest_show_wielded_item>
    * **License:** LGPLv2.1 or later
* `sprint`
    * Allows players to sprint with <kbd>Aux1</kbd>
    * **Upstreams:** <https://github.com/GunshipPenguin/sprint>, <https://github.com/AFCMS/sprint>
    * **License:** CC0
* `stairs`
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/stairs>
    * **License:** LGPLv2.1 or later
* `stairsplus`
    * **License:** zlib/libpng
* `tnt`
    * Adds destructive explosives in the form of TNTs
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/tnt>
    * **License:** MIT
* `tt`
    * Support for custom tooltip extensions for items
    * **Upstream:** <https://codeberg.org/Wuzzy/minetest_tt>
    * **License:** MIT
* `tt_base`
    * Adds generic tooltip extensions to items
    * **Upstream:** <https://codeberg.org/Wuzzy/minetest_tt_base>
    * **License:** MIT
* `vg_alloy`
    * Support for ingot alloying
    * **License:** LGPLv2.1 or later
* `vg_alloy_furnace`
    * Adds a fuel-powered alloy furnace made out of bricks
    * **License:** LGPLv2.1 or later
* `vg_autogroup`
    * Assigns groups to items based on their properties
    * **License:** LGPLv2.1 or later
* `vg_campfire`
    * **License:** LGPLv2.1 or later
* `vg_fallsupport`
    * Generates solid nodes (like stone) under falling nodes using biome
      decorations
    * **License:** LGPLv2.1 or later
* `vg_formspec`
    * Formspec-related utilities
    * **License:** LGPLv2.1 or later
* `vg_hunger`
    * Hunger/satuation mechanics and statbar
    * **License:** LGPLv2.1 or later
* `vg_init`
    * Initialization code, common utilities and APIs
    * **License:** LGPLv2.1 or later
* `vg_item_entity`
    * Replacement for the `builtin` dropped item entity
    * **License:** LGPLv2.1 or later
* `vg_lighting`
    * Applies global lighting effect properties for bloom and dynamic shadows
    * **License:** LGPLv2.1 or later
* `vg_logging`
    * **Upstream:** <https://github.com/C-C-Minetest-Server/logging>
    * **License:** LGPLv2.1 or later
* `vg_mapgen`
    * Basic mapgen aliases, biomes, decorations and ores
    * **License:** LGPLv2.1 or later
* `vg_mobitems`
    * Mob-related items (food and/or crafting material)
    * **License:** LGPLv2.1 or later
* `vg_score`
    * Simple player score mechanics to reward careful, long-running gameplay on
      the same world
    * **License:** LGPLv2.1 or later
* `vg_sky`
    * Colored sky which dynamically changes based on biome
    * **Upstream:** <https://github.com/TerraQuest-Studios/farlands_reloaded/blob/master/mods/fl_mapgen/biome_sky.lua>
    * **License:** MIT
* `vg_tt`
    * Voxelgarden-specific tooltip snippet registrations
    * **License:** LGPLv2.1 or later
* `vg_weather`
    * Weather mechanics and noise-based precipitation (snow or rain, based on
      biome)
    * **Upstreams:** <https://github.com/paramat/snowdrift>, <https://github.com/kestral246/snowdrift>
    * **License:** MIT
* `vessels`
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/vessels>
    * **License:** LGPLv2.1 or later
* `vignette`
    * Optionally adds a vignette effect as a fullscreen overlay
    * **Upstream:** <https://codeberg.org/Wuzzy/Repixture/src/branch/master/mods/rp_vignette>
    * **License:** LGPLv2.1 or later
* `visible_sneak`
    * **Upstream:** <https://github.com/Niwla23/visible_sneak>
    * **License:** MIT
* `walls`
    * Adds connected wall nodes made out of various stones
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/walls>
    * **License:** LGPLv2.1 or later
* `wool`
    * Adds dyeable wool nodes made out of string
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/wool>
    * **License:** MIT
* `xpanes`
    * **Upstream:** <https://github.com/luanti-org/minetest_game/tree/master/mods/xpanes>
    * **License:** MIT
