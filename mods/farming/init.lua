farming = {}

local include = voxelgarden.get_include_helper("farming")

local S = core.get_translator("farming")
local log = voxelgarden.get_logger("farming")

local modules = {
	"api", -- Utilities and general API
	"soil", -- Farmland variants of soil nodes
	"hoes", -- Hoe registrations for Voxelgarden materials
	"legacy" -- Compatibility with old VG `farming`
}

include(modules, {}, S, log)
include({"init"}, {"plants"}, S, log) -- See ./plants/init.lua
