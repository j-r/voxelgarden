local registered_items = minetest.registered_items

minetest.register_on_mods_loaded(function()
	for name, def in pairs(registered_items) do
		local group = def.groups or {}
		group.all = 1

		if def.type == "node" then
			if (def.walkable == nil or def.walkable == true)
					and (def.collision_box == nil or def.collision_box.type == "regular")
					and (def.node_box == nil or def.node_box.type == "regular")
					and (def.groups.not_solid == 0 or def.groups.not_solid == nil) then
				group.solid = 1
			end
			if (not (def.paramtype == "light" or def.sunlight_propagates)) and
					(def.groups.not_opaque == 0 or def.groups.not_opaque == nil) then
				group.opaque = 1
			end
		end
		minetest.override_item(name, {
			groups = group
		})
	end
end)

