-- Load support for MT game translation.
local S = minetest.get_translator("default")

-- HUD alignment
local health_bar_definition = {
	type = "statbar",
	position = { x=0.5, y=1 },
	text = "heart.png",
	number = 20,
	item = 20,
	direction = 0,
	size = {x=16, y=16},
	offset = {x=(-9*24)-12, y=-(48+24+8)},
}

local breath = 20
minetest.register_on_joinplayer(function(player)
	player:set_properties({breath_max = breath})
	player:set_breath(breath)
end)

local breath_bar_definition = {
	type = "statbar",
	position = {x = 0.5, y = 1},
	text = "bubble.png",
	number = breath,
	item = breath,
	direction = 1,
	size = {x = 16, y = 16},
	offset = {x = (9*24)-6, y= -(3*24+8+16)},
}

core.hud_replace_builtin("health", health_bar_definition)
core.hud_replace_builtin("breath", breath_bar_definition)


-- GUI related stuff
function default.get_hotbar_bg(x,y)
	local out = ""
	for i=0,7,1 do
		out = out .."image["..x+i..","..y..";1,1;gui_hb_bg.png]"
	end
	return out
end

default.gui_survival_form = table.concat({
			"size[8,7.5]",
			"list[current_player;main;  0,3.5; 8,1;]",
			"list[current_player;main;  0,4.75; 8,3; 8]",
			"list[current_player;craft; 3,0; 3,3;]",
			"list[current_player;craftpreview; 7,1; 1,1;]",
			"listring[current_player;main]",
			"listring[current_player;craft]",
			default.get_hotbar_bg(0,4.25)
})

-- GUI modes:
--  0 = default (controlled by `vg.accessible_gui`)
--  1 = regular
--  2 = accessible

local DEFAULT = 1
if minetest.settings:get_bool("vg.accessible_gui") then
	DEFAULT = 2
end

local function update_gui(player)
	local meta = player:get_meta()
	if meta:get_int("vg_gui") == 0 then
		meta:set_int("vg_gui", DEFAULT)
	end

	local gui = meta:get_int("vg_gui")

	-- Regular values
	local formbg, hb, hb_selected, gui_slots =
		"gui_formbg.png", "gui_hotbar.png", "gui_hotbar_selected.png",
		"listcolors[#ffffff32;#ffffff77;#ffffff00;#00000088;#FFF]"

	-- Accessibility mode
	if gui == 2 then
		formbg = "gui_formbg_c.png"
		hb, hb_selected = "gui_hotbar_c.png", "gui_hotbar_selected_c.png"
		gui_slots = "listcolors[#ffffff64;#ffffff99;#ffffff00;#000000ee;#FFF]"
	end

	-- Construct the prepend
	local gui_prepend = table.concat({
		"bgcolor[#00000000]",
		"background9[1,1;1,1;", formbg, ";true;16]",
		gui_slots
	})

	-- Setup the GUI
	player:set_formspec_prepend(gui_prepend)
	player:hud_set_hotbar_image(hb)
	player:hud_set_hotbar_selected_image(hb_selected)
end

minetest.register_chatcommand("toggle_gui", {
	description = S("Toggle accessible GUI (ON/OFF)"),
	func = function(name)
		local player = minetest.get_player_by_name(name)
		if not player or not player:is_player() then
			return false
		end

		local meta = player:get_meta()

		local gui = meta:get_int("vg_gui")
		if gui == 1 then
			meta:set_int("vg_gui", 2)
		else
			meta:set_int("vg_gui", 1)
		end

		local mode = S("regular mode")
		if meta:get_int("vg_gui") == 2 then
			mode = S("accessible mode")
		end

		update_gui(player)

		return true, S("GUI has been set to @1.", mode)
	end,
})

minetest.register_on_joinplayer(update_gui)
