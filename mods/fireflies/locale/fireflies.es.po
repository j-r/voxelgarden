msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain fireflies x.x.x\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/fireflies/init.lua:10
msgid "Firefly"
msgstr "Luciérnaga"

#: mods/fireflies/init.lua:48
msgid "Hidden Firefly"
msgstr "Luciérnaga oculta"

#: mods/fireflies/init.lua:75
msgid "Bug Net"
msgstr "Red de insectos"

#: mods/fireflies/init.lua:97
msgid "Firefly in a Bottle"
msgstr "Luciérnaga en botella"
