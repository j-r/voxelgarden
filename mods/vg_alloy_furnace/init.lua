local S = minetest.get_translator("vg_alloy_furnace")

--
-- Formspecs
--

local function active_formspec(fuel_percent, item_percent, pos)
	local formspec =
		"size[8,8.5]"..
		voxelgarden.get_fs_tab(pos)..
		"list[current_name;src;1.5,3;2,1;]"..
		"list[current_name;fuel;3.5,1.5;1,1;]"..
		"image[3.5,0.5;1,1;default_furnace_fire_bg.png^[lowpart:"..
		(100-fuel_percent)..":default_furnace_fire_fg.png]"..
		"image[3.5,3;1,1;gui_furnace_arrow_bg.png^[lowpart:"..
		(item_percent)..":gui_furnace_arrow_fg.png^[transformR270]"..
		"list[current_name;dst;4.5,3;1,1;]"..
		"list[current_player;main;0,4.75;8,4;]"..
		"listring[current_name;dst]"..
		"listring[current_player;main]"..
		"listring[current_name;src]"..
		"listring[current_player;main]"..
		"listring[current_name;fuel]"..
		"listring[current_player;main]"
	return formspec
end

local function inactive_formspec(pos)
	local formspec =
		"size[8,8.5]"..
		voxelgarden.get_fs_tab(pos)..
		"list[current_name;src;1.5,3;2,1;]"..
		"list[current_name;fuel;3.5,1.5;1,1;]"..
		"image[3.5,0.5;1,1;default_furnace_fire_bg.png]"..
		"image[3.5,3;1,1;gui_furnace_arrow_bg.png^[transformR270]"..
		"list[current_name;dst;4.5,3;1,1;]"..
		"list[current_player;main;0,4.75;8,4;]"..
		"listring[current_name;dst]"..
		"listring[current_player;main]"..
		"listring[current_name;src]"..
		"listring[current_player;main]"..
		"listring[current_name;fuel]"..
		"listring[current_player;main]"
	return formspec
end


--
-- Node callback functions that are the same for active and inactive furnace
--

local function can_dig(pos, player)
	local meta = minetest.get_meta(pos);
	local inv = meta:get_inventory()
	return inv:is_empty("fuel") and inv:is_empty("dst") and inv:is_empty("src")
end

local function allow_metadata_inventory_put(pos, listname, index, stack, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return 0
	end
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()
	if listname == "fuel" then
		if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 then
			if inv:is_empty("src") then
				meta:set_string("infotext", S("Alloy Furnace is empty"))
			end
			return stack:get_count()
		else
			return 0
		end
	elseif listname == "src" then
		return stack:get_count()
	elseif listname == "dst" then
		return 0
	end
end

local function allow_metadata_inventory_move(pos, from_list, from_index, to_list, to_index, count, player)
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()
	local stack = inv:get_stack(from_list, from_index)
	return allow_metadata_inventory_put(pos, to_list, to_index, stack, player)
end

local function allow_metadata_inventory_take(pos, listname, index, stack, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return 0
	end
	return stack:get_count()
end

local function swap_node(pos, name)
	local node = minetest.get_node(pos)
	if node.name == name then
		return
	end
	node.name = name
	minetest.swap_node(pos, node)
end

local function get_alloy_result(srclist)
	local name1 = srclist[1]:get_name()
	local name2 = srclist[2]:get_name()
	if (not vg_alloy.materials[name1]) or (not vg_alloy.materials[name2])
			or (vg_alloy.materials[name1] ~= vg_alloy.materials[name2]) then
		return nil, nil
	end
	local result = vg_alloy.materials[name1]
	local recipe = vg_alloy.recipes[result].items
	local time = vg_alloy.recipes[result].time or 3.0
	local list = {name1, name2}
	local identical = ((recipe[1] == list[1])
		and (recipe[2] == list[2])) or
		((recipe[1] == list[2])
		and (recipe[2] == list[1]))
	if not identical then
		return nil, nil
	end

	return ItemStack(result), time
end

local function furnace_node_timer(pos, elapsed)
	-- Inizialize metadata
	local meta = minetest.get_meta(pos)
	local fuel_time = meta:get_float("fuel_time") or 0
	local src_time = meta:get_float("src_time") or 0
	local fuel_totaltime = meta:get_float("fuel_totaltime") or 0
	local inv = meta:get_inventory()
	local srclist, fuellist
	local alloyable
	local fuel
	local update = true
	srclist = inv:get_list("src")
	local result, time
	result, time = get_alloy_result(srclist)
	if not result then
		alloyable = false
	else
		alloyable = true
	end
	while elapsed > 0 and update do
		update = false
		fuellist = inv:get_list("fuel")
		-- Cooking
		-- Check if we have alloyable content
		local el = math.min(elapsed, fuel_totaltime - fuel_time)
		if alloyable then -- fuel lasts long enough, adjust el to cooking duration
			el = math.min(el, time - src_time)
		end
		-- Check if we have enough fuel to burn
		if fuel_time < fuel_totaltime then
			-- The furnace is currently active and has enough fuel
			fuel_time = fuel_time + el
			-- If there is a alloyable item then check if it is ready yet
			if alloyable then
				src_time = src_time + el
				if src_time >= time then
					-- Place result in dst list if possible
					if inv:room_for_item("dst", result) then
						inv:add_item("dst", result)
						local stack1 = inv:get_stack("src", 1)
						local stack2 = inv:get_stack("src", 2)
						stack1:take_item()
						stack2:take_item()
						inv:set_stack("src", 1, stack1)
						inv:set_stack("src", 2, stack2)
						src_time = src_time - time
						update = true
					end
				else
					-- Item could not be alloyed: probably missing fuel
					update = true
				end
			end
		else
			-- Furnace ran out of fuel
			if alloyable then
				-- We need to get new fuel
				local afterfuel
				fuel, afterfuel = minetest.get_craft_result({method = "fuel", width = 1, items = fuellist})
				if fuel.time == 0 then
					-- No valid fuel in fuel list
					fuel_totaltime = 0
					src_time = 0
				else
					-- Take fuel from fuel list
					inv:set_stack("fuel", 1, afterfuel.items[1])
					update = true
					fuel_totaltime = fuel.time + (fuel_totaltime - fuel_time)
				end
			else
				-- We don't need to get new fuel since there is no alloyable item
				fuel_totaltime = 0
				src_time = 0
			end
			fuel_time = 0
		end
		elapsed = elapsed - el
	end
	if fuel and fuel_totaltime > fuel.time then
		fuel_totaltime = fuel.time
	end
	if srclist[1]:is_empty() then
		src_time = 0
	end

	--
	-- Update formspec, infotext and node
	--
	local formspec = inactive_formspec(pos)
	local item_state
	local item_percent = 0
	local fuel_state = S("Empty")
	local active = false
	local result = false
	if alloyable then
		item_percent = math.floor(src_time / time * 100)
		if item_percent > 100 then
			item_state = S("100% (output full)")
		else
			item_state = S("@1%", item_percent)
			active = true
		end
	else
		if srclist[1]:is_empty() then
			item_state = S("Empty")
		else
			item_state = S("Not alloyable")
		end
	end

	if fuel_totaltime ~= 0 then
		local fuel_percent = math.floor(fuel_time / fuel_totaltime * 100)
		fuel_state = S("@1%", fuel_percent)
		formspec = active_formspec(fuel_percent, item_percent, pos)
		swap_node(pos, "vg_alloy_furnace:furnace_active")
		-- make sure timer restarts automatically
		result = true
	else
		if not fuellist[1]:is_empty() then
			fuel_state = "0%"
		end
		swap_node(pos, "vg_alloy_furnace:furnace")
		-- stop timer on the inactive furnace
		minetest.get_node_timer(pos):stop()
	end

	local infotext
	if active then
		infotext = S("Alloy Furnace active")
	else
		infotext = S("Alloy Furnace inactive")
	end
	infotext = infotext .. "\n" .. S("(Item: @1; Fuel: @2)", item_state, fuel_state)

	-- Set meta values
	meta:set_float("fuel_totaltime", fuel_totaltime)
	meta:set_float("fuel_time", fuel_time)
	meta:set_float("src_time", src_time)
	meta:set_string("formspec", formspec)
	meta:set_string("infotext", infotext)

	return result
end

minetest.register_node("vg_alloy_furnace:furnace", {
	description = S("Alloy Furnace"),
	_tt_help = S("Used to alloy ingots"),
	tiles = {
		"vg_alloy_furnace_side.png^[transformR90", "vg_alloy_furnace_side.png",
		"vg_alloy_furnace_side.png", "vg_alloy_furnace_side.png",
		"vg_alloy_furnace_side.png", "vg_alloy_furnace_front.png"
	},
	paramtype2 = "facedir",
	groups = {cracky=3},
	legacy_facedir_simple = true,
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),

	can_dig = can_dig,
	on_timer = furnace_node_timer,
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", inactive_formspec(pos))
		local inv = meta:get_inventory()
		inv:set_size('src', 2)
		inv:set_size('fuel', 1)
		inv:set_size('dst', 1)
	end,

	on_metadata_inventory_move = function(pos)
		minetest.get_node_timer(pos):start(1.0)
	end,
	on_metadata_inventory_put = function(pos)
		-- start timer function, it will sort out whether furnace can burn or not.
		minetest.get_node_timer(pos):start(1.0)
	end,

	allow_metadata_inventory_put = allow_metadata_inventory_put,
	allow_metadata_inventory_move = allow_metadata_inventory_move,
	allow_metadata_inventory_take = allow_metadata_inventory_take,
})

minetest.register_node("vg_alloy_furnace:furnace_active", {
	description = S("Alloy Furnace"),
	tiles = {
		"vg_alloy_furnace_side.png^[transformR90", "vg_alloy_furnace_side.png",
		"vg_alloy_furnace_side.png", "vg_alloy_furnace_side.png",
		"vg_alloy_furnace_side.png",
		{
			image = "vg_alloy_furnace_front_active.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.5
			},
		}
	},
	paramtype2 = "facedir",
	light_source = 8,
	drop = "vg_alloy_furnace:furnace",
	groups = {cracky=3, not_in_creative_inventory=1},
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),

	on_timer = furnace_node_timer,
	can_dig = can_dig,

	allow_metadata_inventory_put = allow_metadata_inventory_put,
	allow_metadata_inventory_move = allow_metadata_inventory_move,
	allow_metadata_inventory_take = allow_metadata_inventory_take,
})

minetest.register_craft({
	output = "vg_alloy_furnace:furnace",
	recipe = {
		{"default:brick", "default:brick", "default:brick"},
		{"default:brick", "",              "default:brick"},
		{"default:brick", "default:brick", "default:brick"},
	}
})
