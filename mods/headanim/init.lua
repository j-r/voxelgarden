local get_connected_players = minetest.get_connected_players
local lastdir = {}

minetest.register_globalstep(function(dtime)
	for _, player in pairs(get_connected_players()) do
		local pname = player:get_player_name()
		local vdir = -player:get_look_vertical()

		if (lastdir[pname] or 0) ~= vdir then
			lastdir[pname] = vdir
			player:set_bone_override("Head", {
				rotation = {
					vec = {x = vdir, y = 0, z = 0}
				}
			})
		end
	end
end)

minetest.register_on_leaveplayer(function(player)
	lastdir[player:get_player_name()] = nil
end)
