msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain game_commands x.x.x\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/game_commands/init.lua:7
msgid "Check game version"
msgstr ""

#: mods/game_commands/init.lua:16
msgid "Respawn"
msgstr ""

#: mods/game_commands/init.lua:32
msgid "No static_spawnpoint defined"
msgstr "Pas de point d'apparition défini"

#: mods/game_commands/init.lua:36
msgid "You need to be online to respawn!"
msgstr ""
