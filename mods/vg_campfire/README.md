# `vg_campfire`

## License of source code

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

http://www.gnu.org/licenses/lgpl-2.1.html


## License of media

Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)

http://creativecommons.org/licenses/by-sa/4.0/

rudzik8:
* `vg_campfire_campfire.png` (based on textures by Casimir and Gambit)
* `vg_campfire_campfire_unlit.png` (based on textures by Casimir and Gambit)

Gambit:
  `vg_campfire_campfire_animated.png`
