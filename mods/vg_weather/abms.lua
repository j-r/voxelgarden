if minetest.get_modpath("fire") then
	minetest.register_abm({
		label = "Extinguish fire under rain/snow",
		nodenames = {"fire:basic_flame"},
		interval = 6,
		chance = 2,
		min_y = vg_weather.min_y,
		max_y = vg_weather.max_y,
		action = function(pos)
			local apos = vector.copy(pos)
			apos.y = apos.y + 1
			if minetest.get_node_light(apos, 0.5) == 15 and voxelgarden.get_weather(pos) ~= "none" then
				minetest.sound_play("fire_extinguish_flame",
					{pos = pos, max_hear_distance = 16, gain = 0.15}, true)
				minetest.remove_node(pos)
			end
		end
	})
end

if minetest.get_modpath("farming") then
	minetest.register_abm({
		label = "Wetten soil under rain",
		nodenames = {"group:field"},
		inteval = 6,
		chance = 2,
		min_y = vg_weather.min_y,
		max_y = vg_weather.max_y,
		action = function(pos, node)
			local def = minetest.registered_nodes[node.name]
			if not (def.soil and def.soil.wet) then return end

			local apos = vector.copy(pos)
			apos.y = apos.y + 1
			if minetest.get_node_light(apos, 0.5) == 15 and voxelgarden.get_weather(pos) == "rain" then
				minetest.swap_node(pos, {name = def.soil.wet})
			end
		end
	})
end

if minetest.settings:get_bool("vg.layer_snow", true) then
	minetest.register_abm({
		label = "Snow",
		nodenames = {"group:solid"},
		neighbors = {"air"},
		interval = 18,
		chance = 6,
		min_y = vg_weather.min_y,
		max_y = vg_weather.max_y,
		action = function(pos, node)
			local apos = vector.copy(pos)
			apos.y = apos.y + 1
			if  minetest.get_node_light(apos, 0.5) == 15
					and voxelgarden.get_weather(apos) == "snow"
					and minetest.get_node(apos).name == "air" then
				minetest.set_node(apos, {name = "default:snow"})
			end
		end
	})
end
