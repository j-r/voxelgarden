Voxelgarden mod: vessels
==========================
See license.txt for license information.

Authors of source code
----------------------
Originally by Vanessa Ezekowitz (LGPLv2.1+)
Modified by Perttu Ahola <celeron55@gmail.com> (LGPLv2.1+)
Various Minetest developers and contributors (LGPLv2.1+)

Authors of media (textures)
---------------------------
Gambit (CC BY-SA 4.0):
  vessels_drinking_glass.png
  vessels_drinking_glass_inv.png
  vessels_glass_bottle.png
  vessels_glass_fragments.png
  vessels_steel_bottle.png

rudzik8 (CC BY-SA 4.0):
  vessels_shelf.png (based on default_wood.png and texture from Vanessa Ezekowitz)
  vessels_shelf_slot.png (based on vessels_glass_bottle.png)
