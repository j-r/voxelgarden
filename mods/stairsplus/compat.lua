-- Functions and wrappers to provide support for More Blocks Stairplus API

local S = minetest.get_translator("stairsplus")

function stairsplus:register_all(modname, subname, recipeitem, fields)
	local desc = fields.description
	local desc_stair = S("@1 Stair", desc)
	local desc_corner = S("@1 Corner", desc)
	local desc_slab = S("@1 Slab", desc)
	local desc_wall = S("@1 Wall", desc)
	local desc_panel = S("@1 Panel", desc)
	local desc_micro = S("@1 Microblock", desc)

	-- use recipeitem as drop if no drop provided
	local drop = (fields.drop or recipeitem)

	self.register_stair_and_slab_and_panel_and_micro(
		modname,
		subname,
		recipeitem,
		fields.groups,
		fields.tiles,
		desc_stair,
		desc_corner,
		desc_slab,
		desc_wall,
		desc_panel,
		desc_micro,
		drop,
		fields.sounds,
		true
	)
end

--[[ TODO
function stairsplus:register_stair(modname, subname, recipeitem, fields)
	local desc = S("@1 Stair", fields.description)

	-- use recipeitem as drop if no drop provided
	local drop = (fields.drop or recipeitem)

	self.register_stair(
		modname,
		subname,
		recipeitem,
		fields.groups,
		fields.tiles,
		desc,
		drop,
		fields.sounds,
		true
	)
end

function stairsplus:register_slab(modname, subname, recipeitem, fields)
	local desc = S("@1 Slab", fields.description)

	-- use recipeitem as drop if no drop provided
	local drop = (fields.drop or recipeitem)

	self.register_slab(
		modname,
		subname,
		recipeitem,
		fields.groups,
		fields.tiles,
		desc,
		drop,
		fields.sounds,
		true
	)
end

function stairsplus:register_panel(modname, subname, recipeitem, fields)
	local desc = S("@1 Panel", fields.description)

	-- use recipeitem as drop if no drop provided
	local drop = (fields.drop or recipeitem)

	self.register_panel(
		modname,
		subname,
		recipeitem,
		fields.groups,
		fields.tiles,
		desc,
		drop,
		fields.sounds,
		true
	)

end

function stairsplus:register_micro(modname, subname, recipeitem, fields)
	local desc = S("@1 Microblock", fields.description)

	-- use recipeitem as drop if no drop provided
	local drop = (fields.drop or recipeitem)

	self.register_micro(
		modname,
		subname,
		recipeitem,
		fields.groups,
		fields.tiles,
		desc,
		drop,
		fields.sounds,
		true
	)

end

function stairsplus:register_slope(modname, subname, recipeitem, fields)
	local desc = S("@1 Slope", fields.description)

	-- use recipeitem as drop if no drop provided
	local drop = (fields.drop or recipeitem)

	self.register_slope(
		modname,
		subname,
		recipeitem,
		fields.groups,
		fields.tiles,
		desc,
		drop,
		fields.sounds,
		true
	)
end]]
