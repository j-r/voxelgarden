# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Voxelgarden package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Voxelgarden\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/stairsplus/compat.lua:7
msgid "@1 Stair"
msgstr ""

#: mods/stairsplus/compat.lua:8
msgid "@1 Corner"
msgstr ""

#: mods/stairsplus/compat.lua:9
msgid "@1 Slab"
msgstr ""

#: mods/stairsplus/compat.lua:10
msgid "@1 Wall"
msgstr ""

#: mods/stairsplus/compat.lua:11
msgid "@1 Panel"
msgstr ""

#: mods/stairsplus/compat.lua:12
msgid "@1 Microblock"
msgstr ""

#: mods/stairsplus/init.lua:29
msgid "Apple Wood Stairs"
msgstr ""

#: mods/stairsplus/init.lua:30
msgid "Apple Wood Corner"
msgstr ""

#: mods/stairsplus/init.lua:31
msgid "Apple Wood Slab"
msgstr ""

#: mods/stairsplus/init.lua:32
msgid "Apple Wood Wall"
msgstr ""

#: mods/stairsplus/init.lua:33
msgid "Apple Wood Panel"
msgstr ""

#: mods/stairsplus/init.lua:34
msgid "Apple Wood Microblock"
msgstr ""

#: mods/stairsplus/init.lua:42
msgid "Jungle Wood Stairs"
msgstr ""

#: mods/stairsplus/init.lua:43
msgid "Jungle Wood Corner"
msgstr ""

#: mods/stairsplus/init.lua:44
msgid "Jungle Wood Slab"
msgstr ""

#: mods/stairsplus/init.lua:45
msgid "Jungle Wood Wall"
msgstr ""

#: mods/stairsplus/init.lua:46
msgid "Jungle Wood Panel"
msgstr ""

#: mods/stairsplus/init.lua:47
msgid "Jungle Wood Microblock"
msgstr ""

#: mods/stairsplus/init.lua:55
msgid "Pine Wood Stairs"
msgstr ""

#: mods/stairsplus/init.lua:56
msgid "Pine Wood Corner"
msgstr ""

#: mods/stairsplus/init.lua:57
msgid "Pine Wood Slab"
msgstr ""

#: mods/stairsplus/init.lua:58
msgid "Pine Wood Wall"
msgstr ""

#: mods/stairsplus/init.lua:59
msgid "Pine Wood Panel"
msgstr ""

#: mods/stairsplus/init.lua:60
msgid "Pine Wood Microblock"
msgstr ""

#: mods/stairsplus/init.lua:68
msgid "Acacia Wood Stairs"
msgstr ""

#: mods/stairsplus/init.lua:69
msgid "Acacia Wood Corner"
msgstr ""

#: mods/stairsplus/init.lua:70
msgid "Acacia Wood Slab"
msgstr ""

#: mods/stairsplus/init.lua:71
msgid "Acacia Wood Wall"
msgstr ""

#: mods/stairsplus/init.lua:72
msgid "Acacia Wood Panel"
msgstr ""

#: mods/stairsplus/init.lua:73
msgid "Acacia Wood Microblock"
msgstr ""

#: mods/stairsplus/init.lua:81
msgid "Birch Wood Stairs"
msgstr ""

#: mods/stairsplus/init.lua:82
msgid "Birch Wood Corner"
msgstr ""

#: mods/stairsplus/init.lua:83
msgid "Birch Wood Slab"
msgstr ""

#: mods/stairsplus/init.lua:84
msgid "Birch Wood Wall"
msgstr ""

#: mods/stairsplus/init.lua:85
msgid "Birch Wood Panel"
msgstr ""

#: mods/stairsplus/init.lua:86
msgid "Birch Wood Microblock"
msgstr ""

#: mods/stairsplus/init.lua:94
msgid "Stone Stairs"
msgstr ""

#: mods/stairsplus/init.lua:95
msgid "Stone Corner"
msgstr ""

#: mods/stairsplus/init.lua:96
msgid "Stone Slab"
msgstr ""

#: mods/stairsplus/init.lua:97
msgid "Stone Wall"
msgstr ""

#: mods/stairsplus/init.lua:98
msgid "Stone Panel"
msgstr ""

#: mods/stairsplus/init.lua:99
msgid "Stone Microblock"
msgstr ""

#: mods/stairsplus/init.lua:107
msgid "Hardened Rock Stairs"
msgstr ""

#: mods/stairsplus/init.lua:108
msgid "Hardened Rock Corner"
msgstr ""

#: mods/stairsplus/init.lua:109
msgid "Hardened Rock Slab"
msgstr ""

#: mods/stairsplus/init.lua:110
msgid "Hardened Rock Wall"
msgstr ""

#: mods/stairsplus/init.lua:111
msgid "Hardened Rock Panel"
msgstr ""

#: mods/stairsplus/init.lua:112
msgid "Hardened Rock Microblock"
msgstr ""

#: mods/stairsplus/init.lua:120
msgid "Crumbled Stairs"
msgstr ""

#: mods/stairsplus/init.lua:121
msgid "Crumbled Corner"
msgstr ""

#: mods/stairsplus/init.lua:122
msgid "Crumbled Slab"
msgstr ""

#: mods/stairsplus/init.lua:123
msgid "Crumbled Wall"
msgstr ""

#: mods/stairsplus/init.lua:124
msgid "Crumbled Panel"
msgstr ""

#: mods/stairsplus/init.lua:125
msgid "Crumbled Microblock"
msgstr ""

#: mods/stairsplus/init.lua:133
msgid "Cobblestone Stairs"
msgstr ""

#: mods/stairsplus/init.lua:134
msgid "Cobblestone Corner"
msgstr ""

#: mods/stairsplus/init.lua:135
msgid "Cobblestone Slab"
msgstr ""

#: mods/stairsplus/init.lua:136
msgid "Cobblestone Wall"
msgstr ""

#: mods/stairsplus/init.lua:137
msgid "Cobblestone Panel"
msgstr ""

#: mods/stairsplus/init.lua:138
msgid "Cobblestone Microblock"
msgstr ""

#: mods/stairsplus/init.lua:146
msgid "Brick Stairs"
msgstr ""

#: mods/stairsplus/init.lua:147
msgid "Brick Corner"
msgstr ""

#: mods/stairsplus/init.lua:148
msgid "Brick Slab"
msgstr ""

#: mods/stairsplus/init.lua:149
msgid "Brick Wall"
msgstr ""

#: mods/stairsplus/init.lua:150
msgid "Brick Panel"
msgstr ""

#: mods/stairsplus/init.lua:151
msgid "Brick Microblock"
msgstr ""

#: mods/stairsplus/init.lua:159
msgid "Iron Block Stairs"
msgstr ""

#: mods/stairsplus/init.lua:160
msgid "Iron Block Corner"
msgstr ""

#: mods/stairsplus/init.lua:161
msgid "Iron Block Slab"
msgstr ""

#: mods/stairsplus/init.lua:162
msgid "Iron Block Wall"
msgstr ""

#: mods/stairsplus/init.lua:163
msgid "Iron Block Panel"
msgstr ""

#: mods/stairsplus/init.lua:164
msgid "Iron Microblock"
msgstr ""

#: mods/stairsplus/init.lua:172
msgid "Steel Block Stairs"
msgstr ""

#: mods/stairsplus/init.lua:173
msgid "Steel Block Corner"
msgstr ""

#: mods/stairsplus/init.lua:174
msgid "Steel Block Slab"
msgstr ""

#: mods/stairsplus/init.lua:175
msgid "Steel Block Wall"
msgstr ""

#: mods/stairsplus/init.lua:176
msgid "Steel Block Panel"
msgstr ""

#: mods/stairsplus/init.lua:177
msgid "Steel Microblock"
msgstr ""

#: mods/stairsplus/init.lua:185
msgid "Desert Stone Stairs"
msgstr ""

#: mods/stairsplus/init.lua:186
msgid "Desert Stone Corner"
msgstr ""

#: mods/stairsplus/init.lua:187
msgid "Desert Stone Slab"
msgstr ""

#: mods/stairsplus/init.lua:188
msgid "Desert Stone Wall"
msgstr ""

#: mods/stairsplus/init.lua:189
msgid "Desert Stone Panel"
msgstr ""

#: mods/stairsplus/init.lua:190
msgid "Desert Stone Microblock"
msgstr ""

#: mods/stairsplus/init.lua:198
msgid "Glass Stairs"
msgstr ""

#: mods/stairsplus/init.lua:199
msgid "Glass Corner"
msgstr ""

#: mods/stairsplus/init.lua:200
msgid "Glass Slab"
msgstr ""

#: mods/stairsplus/init.lua:201
msgid "Glass Wall"
msgstr ""

#: mods/stairsplus/init.lua:202
msgid "Glass Panel"
msgstr ""

#: mods/stairsplus/init.lua:203
msgid "Glass Microblock"
msgstr ""

#: mods/stairsplus/init.lua:211
msgid "Sandstone Stairs"
msgstr ""

#: mods/stairsplus/init.lua:212
msgid "Sandstone Corner"
msgstr ""

#: mods/stairsplus/init.lua:213
msgid "Sandstone Slab"
msgstr ""

#: mods/stairsplus/init.lua:214
msgid "Sandstone Wall"
msgstr ""

#: mods/stairsplus/init.lua:215
msgid "Sandstone Panel"
msgstr ""

#: mods/stairsplus/init.lua:216
msgid "Sandstone Microblock"
msgstr ""

#: mods/stairsplus/init.lua:224
msgid "Stone Brick Stairs"
msgstr ""

#: mods/stairsplus/init.lua:225
msgid "Stone Brick Corner"
msgstr ""

#: mods/stairsplus/init.lua:226
msgid "Stone Brick Slab"
msgstr ""

#: mods/stairsplus/init.lua:227
msgid "Stone Brick Wall"
msgstr ""

#: mods/stairsplus/init.lua:228
msgid "Stone Brick Panel"
msgstr ""

#: mods/stairsplus/init.lua:229
msgid "Stone Brick Microblock"
msgstr ""
