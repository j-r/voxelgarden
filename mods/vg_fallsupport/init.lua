minetest.register_decoration({
	deco_type = "simple",
	place_on = {"group:falling_node"},
	sidelen = 16,
	fill_ratio = 10.0, -- complete coverage
	decoration = {"default:sandstone"},
	y_min = -31000,
	y_max = 31000,
	flags = "all_ceilings",
	biomes = {"sandstone_desert"},
})

minetest.register_decoration({
	deco_type = "simple",
	place_on = {"group:falling_node"},
	sidelen = 16,
	fill_ratio = 10.0, -- complete coverage
	decoration = {"default:desert_stone"},
	y_min = -31000,
	y_max = 31000,
	flags = "all_ceilings",
	biomes = {"savanna", "desert", "rocky_desert"},
})

-- everything else
minetest.register_decoration({
	deco_type = "simple",
	place_on = {"group:falling_node"},
	sidelen = 16,
	fill_ratio = 10.0, -- complete coverage
	decoration = {
		"default:stone",
		"default:stone_crumbled",
	},
	y_min = -31000,
	y_max = 31000,
	flags = "all_ceilings",
})
