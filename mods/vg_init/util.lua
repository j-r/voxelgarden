-- Helper function wrapper for bulk module loading with an option to share local
-- variables between them via loadfile.
function voxelgarden.get_include_helper(modname)
	modname = modname or core.get_current_modname()
	-- * `files` is a table containing base filenames without ".lua"
	-- * `basedir` is a table listing out the directories that have to be traversed
	--   to get to the files
	-- * `...` is everything you want to share with the loaded files (vararg)
	return function(files, basedir, ...)
		for _, file in ipairs(files) do
			loadfile(table.concat({
				core.get_modpath(modname),
				table.concat(basedir, DIR_DELIM),
				file .. ".lua"
			}, DIR_DELIM))(...)
		end
	end
	-- Usage example:
	--	local S = core.get_translator("mymod")
	--	local include = voxelgarden.get_include_helper("mymod")
	--	local modules = {"common", "api", "register"}
	--	include(modules, {"src"}, S)
	--		--> src/common.lua, src/api.lua, src/register.lua
	--		--> Inside the files:
	--		-->	local S = ... -- fetch variable(-s) from the loader
end

-- Call `on_rightclick` of the pointed node if defined.
function voxelgarden.call_on_rightclick(itemstack, player, pointed_thing)
	-- Only proceed if the following conditions are met:
	--  * `pointed_thing` isn't nil;
	--  * `pointed_thing` is a node;
	--  * `player` isn't nil;
	--  * `player` is a valid ObjectRef to a player.
	--  * `player` isn't sneaking (sneak-place feature).
	if (not pointed_thing) or (pointed_thing.type ~= "node")
			or (
				player and player:is_player()
				and player:get_player_control().sneak
			) then
		return
	end

	-- If the pointed node isn't loaded (WTF?), don't proceed either.
	local node = core.get_node_or_nil(pointed_thing.under)
	if not node then return end

	-- Finally, call `on_rightclick` if defined.
	-- Return the original itemstack if the pointed node's
	-- `on_rightclick` doesn't return anything.
	local def = core.registered_nodes[node.name]
	if def and def.on_rightclick then
		return def.on_rightclick(pointed_thing.under, node, player, itemstack, pointed_thing)
			or itemstack
	end
end
