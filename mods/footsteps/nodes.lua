local S = minetest.get_translator("default")

minetest.register_node(":default:dirt_with_grass_footsteps", {
	description = S("Dirt with Grass and Footsteps"),
	tiles = {"default_grass.png^footsteps.png", "default_dirt.png", "default_dirt.png^default_grass_side.png"},
	groups = {crumbly=3, not_in_creative_inventory=1, falling_node=1, soil=1, spreading_dirt_type = 1},
	drop = 'default:dirt',
	sounds = default.node_sound_dirt_defaults({
		footstep = {name="default_grass_footstep", gain=0.1},
	}),
	_footsteps = true,
	_footsteps_name = "default:dirt_with_grass",
})
minetest.override_item("default:dirt_with_grass", {_footsteps_name = "default:dirt_with_grass_footsteps"})

minetest.register_node(":default:dirt_with_dry_grass_footsteps", {
	description = S("Dirt with Savanna Grass and Footsteps"),
	tiles = {"default_dry_grass.png^footsteps.png", "default_dirt.png", "default_dirt.png^default_dry_grass_side.png"},
	groups = {crumbly=3, not_in_creative_inventory=1, falling_node=1, soil=1, spreading_dirt_type = 1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name="default_grass_footstep", gain=0.1},
	}),
	_footsteps = true,
	_footsteps_name = "default:dirt_with_dry_grass",
})
minetest.override_item("default:dirt_with_dry_grass", {_footsteps_name = "default:dirt_with_dry_grass_footsteps"})

minetest.register_node(":default:dry_dirt_with_dry_grass_footsteps", {
	description = S("Savanna Dirt with Savanna Grass and Footsteps"),
	tiles = {
		"default_dry_grass.png^footsteps.png",
		"default_dry_dirt.png",
		"default_dry_dirt.png^default_dry_grass_side.png"
	},
	groups = {crumbly=3, not_in_creative_inventory=1, falling_node=1, soil=1, spreading_dry_dirt_type = 1},
	drop = "default:dry_dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name="default_grass_footstep", gain=0.1},
	}),
	_footsteps = true,
	_footsteps_name = "default:dry_dirt_with_dry_grass",
})
minetest.override_item("default:dry_dirt_with_dry_grass",
	{_footsteps_name = "default:dry_dirt_with_dry_grass_footsteps"})

minetest.register_node(":default:dirt_with_snow_footsteps", {
	description = S("Dirt with Snow and Footsteps"),
	tiles = {"default_snow.png^footsteps.png", "default_dirt.png", "default_dirt.png^default_snow_side.png"},
	groups = {crumbly=3, not_in_creative_inventory=1, falling_node=1, soil=1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name="default_grass_footstep", gain=0.1},
	}),
	_footsteps = true,
	_footsteps_name = "default:dirt_with_snow",
})
minetest.override_item("default:dirt_with_snow", {_footsteps_name = "default:dirt_with_snow_footsteps"})

minetest.register_node(":default:sand_footsteps", {
	description = S("Sand with Footsteps"),
	tiles = {"default_sand.png^footsteps.png", "default_sand.png"},
	groups = {crumbly=3, not_in_creative_inventory=1, falling_node=1, sand=1},
	sounds = default.node_sound_sand_defaults(),
	drop = "default:sand",
	_footsteps = true,
	_footsteps_name = "default:sand",
})
minetest.override_item("default:sand", {_footsteps_name = "default:sand_footsteps"})

minetest.register_node(":default:desert_sand_footsteps", {
	description = S("Desert Sand with Footsteps"),
	tiles = {"default_desert_sand.png^footsteps.png", "default_desert_sand.png"},
	groups = {crumbly=3, not_in_creative_inventory=1, falling_node=1, sand=1},
	sounds = default.node_sound_sand_defaults(),
	drop = "default:desert_sand",
	_footsteps = true,
	_footsteps_name = "default:desert_sand",
})
minetest.override_item("default:desert_sand", {_footsteps_name = "default:desert_sand_footsteps"})

minetest.register_node(":default:gravel_footsteps", {
	description = S("Gravel with Footsteps"),
	tiles = {"default_gravel.png^footsteps.png", "default_gravel.png"},
	groups = {crumbly=2, not_in_creative_inventory=1, falling_node=1},
	sounds = default.node_sound_gravel_defaults(),
	drop = {
		max_items = 1,
		items = {
			{items = {"default:flint"}, rarity = 16},
			{items = {"default:gravel"}}
		}
	},
	_footsteps = true,
	_footsteps_name = "default:gravel",
})
minetest.override_item("default:gravel", {_footsteps_name = "default:gravel_footsteps"})

minetest.register_node(":default:snowblock_footsteps", {
	description = S("Snow Block with Footsteps"),
	tiles = {"default_snow.png^footsteps.png", "default_snow.png"},
	groups = {crumbly=3, not_in_creative_inventory=1, falling_node=1,
		cools_lava=1, fall_damage_add_percent=default.COUSHION,},
	sounds = default.node_sound_snow_defaults(),
	drop = "default:snowblock",
	_footsteps = true,
	_footsteps_name = "default:snowblock",
})
minetest.override_item("default:snowblock", {_footsteps_name = "default:snowblock_footsteps"})
