msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain bones x.x.x\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/bones/init.lua:64
msgid "Bones"
msgstr "Кости"

#: mods/bones/init.lua:65
msgid "Contains dead player's items"
msgstr ""

#: mods/bones/init.lua:162
msgid "@1's old bones"
msgstr "Старые кости @1"

#: mods/bones/init.lua:234 mods/bones/init.lua:243
msgid "@1 died at @2."
msgstr ""

#: mods/bones/init.lua:268
msgid "@1 died at @2, and dropped their inventory."
msgstr ""

#: mods/bones/init.lua:278
msgid "@1 died at @2, and bones were placed."
msgstr ""

#: mods/bones/init.lua:301
msgid "@1's fresh bones"
msgstr "Новые кости @1"

#: mods/bones/init.lua:311
msgid "@1's bones"
msgstr "Кости @1"
