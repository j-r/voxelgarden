stairs = {}

function stairs.register_stair(subname, recipeitem, groups, images, description, sounds)
	groups.not_in_craft_guide = 1
	stairsplus.register_stair(
		"stairs", subname, recipeitem, groups, images, description, subname, sounds, false
	)
end

function stairs.register_stair_inner(subname, recipeitem, groups, images, description, sounds, worldaligntex,
		full_decription)
	if not minetest.registered_nodes["stairs:stair_"..subname] then
		stairsplus.register_stair(
			"stairs", subname, recipeitem, groups, images, description, subname, sounds, false
		)
	end
end

function stairs.register_stair_outer(subname, recipeitem, groups, images, description, sounds, worldaligntex,
		full_decription)
	if not minetest.registered_nodes["stairs:stair_"..subname] then
		stairsplus.register_stair(
			"stairs", subname, recipeitem, groups, images, description, subname, sounds, false
		)
	end
end

function stairs.register_slab(subname, recipeitem, groups, images, description, sounds)
	groups.not_in_craft_guide = 1
	stairsplus.register_slab(
		"stairs", subname, recipeitem, groups, images, description, subname, sounds, false
	)
end

function stairs.register_stair_and_slab(subname, recipeitem, groups, images, desc_stair, desc_slab, sounds)
	stairs.register_stair(subname, recipeitem, groups, images, desc_stair, sounds)
	stairs.register_slab(subname, recipeitem, groups, images, desc_slab, sounds)
end
