msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain butterflies x.x.x\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/butterflies/init.lua:11
msgid "Yellow Butterfly"
msgstr ""

#: mods/butterflies/init.lua:12
msgid "Red Butterfly"
msgstr "红蝴蝶"

#: mods/butterflies/init.lua:13
msgid "Violet Butterfly"
msgstr "紫蝴蝶"
