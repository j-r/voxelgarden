-- Load support for MT game translation.
local S = minetest.get_translator("conifer")

conifer = {}

minetest.register_alias("mapgen_pine_tree", "conifer:tree")
minetest.register_alias("mapgen_pine_needles", "conifer:leaves_1")

--
-- Nodes
--

minetest.register_node("conifer:sapling", {
	description = S("Conifer Sapling"),
	drawtype = "plantlike",
	tiles = {"conifer_sapling.png"},
	inventory_image = "conifer_sapling.png",
	wield_image = "conifer_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	floodable = true,
	on_timer = function(pos)
		conifer.grow_conifersapling(pos)
	end,
	selection_box = {
		type = "fixed",
		fixed = {-0.3, -0.5, -0.3, 0.3, 0.35, 0.3}
	},
	groups = {snappy=2, dig_immediate=3, sapling=1, flammable=2, falling_node=1},
	sounds = default.node_sound_leaves_defaults(),
	on_construct = function(pos)
		minetest.get_node_timer(pos):start(math.random(600, 9600))
	end,
})

minetest.register_node("conifer:tree", {
	description = S("Conifer Tree"),
	tiles = {"conifer_tree_top.png", "conifer_tree_top.png", "conifer_tree.png"},
	is_ground_content = false,
	groups = {tree=1, choppy=2, flammable=1},
	sounds = default.node_sound_wood_defaults(),
})

minetest.register_node("conifer:wood", {
	description = S("Conifer Wood"),
	tiles = {"conifer_wood.png"},
	is_ground_content = false,
	groups = {choppy=2, oddly_breakable_by_hand=1, flammable=3, wood=1},
	sounds = default.node_sound_wood_defaults(),
})

for i=1,2 do
	minetest.register_node("conifer:leaves_"..i, {
		description = S("Conifer Leaves"),
		drawtype = "allfaces_optional",
		tiles = {"conifer_leaves_"..i..".png"},
		waving = 1,
		paramtype = "light",
		place_param2 = 1,
		is_ground_content = false,
		trunk = "conifer:tree",
		groups = {
				snappy=3,
				leafdecay=4,
				flammable=2,
				leaves=1,
				fall_damage_add_percent=default.COUSHION
			},
		drop = {
			max_items = 1,
			items = {
				{
					-- player will get sapling with 1/rarity chance
					items = {'conifer:sapling'},
					rarity = 40,
				},
				{
					-- player will get leaves only if he get no saplings,
					-- this is because max_items is 1
					items = {"conifer:leaves_"..i..""},
				}
			}
		},
		sounds = default.node_sound_leaves_defaults(),
	})
end

minetest.register_alias("conifers:leaves", "conifer:leaves_1")
minetest.register_alias("conifers:leaves_special", "conifer:leaves_2")

minetest.register_node("conifer:tree_horizontal", {
	description = S("Conifer Tree"),
	tiles = {
		"conifer_tree.png",
		"conifer_tree.png",
		"conifer_tree.png^[transformR90",
		"conifer_tree.png^[transformR90",
		"conifer_tree_top.png",
		"conifer_tree_top.png"
	},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree_horizontal=1, choppy=2, flammable=1},
	melt = "default:coal_block",
	sounds = default.node_sound_wood_defaults(),
	on_construct = function(pos)
		default.rotate_horizontal(pos)
	end,
})

default.register_mesepost("conifer:mese_post_light_wood", {
	description = S("Conifer Mese Post"),
	texture = "conifer_wood.png^[transformR90",
	material = "conifer:wood",
})

default.register_fence("conifer:fence_wood", {
	description = S("Conifer Wood Fence"),
	texture = "conifer_wood.png^[transformR90",
	inventory_image = "default_fence_overlay.png^conifer_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_overlay.png^conifer_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	material = "conifer:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence_rail("conifer:fence_rail_wood", {
	description = S("Conifer Wood Fence Rail"),
	texture = "conifer_wood.png^[transformR90",
	inventory_image = "default_fence_rail_overlay.png^conifer_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_rail_overlay.png^conifer_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	material = "conifer:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = default.node_sound_wood_defaults()
})

doors.register_fencegate("conifer:gate_wood", {
	description = S("Conifer Wood Gate"),
	texture = "conifer_wood.png",
	material = "conifer:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2}
})

stairsplus.register_stair_and_slab_and_panel_and_micro("conifer", "wood", "conifer:wood",
		{snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3,not_in_craft_guide=1},
		{"conifer_wood.png"},
		S("Conifer Wood Stairs"),
		S("Conifer Wood Corner"),
		S("Conifer Wood Slab"),
		S("Conifer Wood Wall"),
		S("Conifer Wood Panel"),
		S("Conifer Wood Microblock"),
		"wood",
		default.node_sound_wood_defaults()
		)

--
-- Crafting definition
--

minetest.register_craft({
	output = 'conifer:wood 2',
	recipe = {
		{'conifer:tree'},
	}
})

minetest.register_craft({
	output = 'conifer:wood 2',
	recipe = {
		{'conifer:tree_horizontal'},
	}
})

minetest.register_craft({
	output = 'conifer:tree_horizontal 2',
	recipe = {
		{'', 'conifer:tree'},
		{'conifer:tree', ''},
	}
})

minetest.register_craft({
	output = 'conifer:tree 2',
	recipe = {
		{'', 'conifer:tree_horizontal'},
		{'conifer:tree_horizontal', ''},
	}
})

dofile(minetest.get_modpath("conifer").."/trees.lua")
