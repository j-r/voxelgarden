local S = core.get_translator("vg_tt")

-- Container
tt.register_snippet(function(itemstring)
       local def = core.registered_items[itemstring]
       local desc
       if def._tt_container then
               desc = S("Container")
       end
       return desc
end)

-- Igniteable
tt.register_snippet(function(itemstring)
       local def = core.registered_items[itemstring]
       local desc
       if def.on_ignite then
               desc = S("Can be ignited")
       end
       return desc
end)

-- Dyeable
tt.register_snippet(function(itemstring)
       local def = core.registered_items[itemstring]
       local desc
       if def._dyestr then
               desc = S("Can be dyed")
       end
       return desc
end)
