msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain wool x.x.x\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/wool/init.lua:14
msgid "White Wool"
msgstr "Weiße Wolle"

#: mods/wool/init.lua:15
msgid "Grey Wool"
msgstr "Graue Wolle"

#: mods/wool/init.lua:16
msgid "Black Wool"
msgstr "Schwarze Wolle"

#: mods/wool/init.lua:17
msgid "Red Wool"
msgstr "Rote Wolle"

#: mods/wool/init.lua:18
msgid "Yellow Wool"
msgstr "Gelbe Wolle"

#: mods/wool/init.lua:19
msgid "Green Wool"
msgstr "Grüne Wolle"

#: mods/wool/init.lua:20
msgid "Cyan Wool"
msgstr "Türkise Wolle"

#: mods/wool/init.lua:21
msgid "Blue Wool"
msgstr "Blaue Wolle"

#: mods/wool/init.lua:22
msgid "Magenta Wool"
msgstr "Magenta Wolle"

#: mods/wool/init.lua:23
msgid "Orange Wool"
msgstr "Orange Wolle"

#: mods/wool/init.lua:24
msgid "Violet Wool"
msgstr "Violette Wolle"

#: mods/wool/init.lua:25
msgid "Brown Wool"
msgstr "Braune Wolle"

#: mods/wool/init.lua:26
msgid "Pink Wool"
msgstr "Rosa Wolle"

#: mods/wool/init.lua:27
msgid "Dark Grey Wool"
msgstr "Dunkelgraue Wolle"

#: mods/wool/init.lua:28
msgid "Dark Green Wool"
msgstr "Dunkelgrüne Wolle"
