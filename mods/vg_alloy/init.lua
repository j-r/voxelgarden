vg_alloy = {
	recipes = {},
	materials = {},
}

local log = voxelgarden.get_logger("vg_alloy")

---- Input (voxelgarden.register_alloy):
-- {
-- 	output = "mod:magic_ingot",
-- 	items = {"mod:foo_ingot", "mod:bar_ingot"},
-- 	time = 5.0,
-- }
--
---- Output (vg_alloy.recipes):
-- ["mod:magic_ingot"] = {
--	output = "mod:magic_ingot",
-- 	method = "alloying", -- craftguide
-- 	width = 2,           -- craftguide
-- 	items = {"mod:foo_ingot", "mod:bar_ingot"},
-- 	time = 5.0,
-- }
function voxelgarden.register_alloy(t)
	if (not t) or (not t.output) or (not t.items)
			or (type(t.output) ~= "string")
			or (type(t.items) ~= "table") then
		log:raise("Invalid alloying recipe")
	end
	-- Assign default time
	if not t.time then t.time = 3.0 end

	t.method = "alloying"
	t.width = 2
	vg_alloy.recipes[t.output] = t

	for i=1,#t.items do
		vg_alloy.materials[t.items[i]] = t.output
	end
end
