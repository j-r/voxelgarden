# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Voxelgarden package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Voxelgarden\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/creative/init.lua:23
msgid "Allow player to use creative inventory"
msgstr ""

#: mods/creative/inventory.lua:158
msgid "No items to show."
msgstr ""

#: mods/creative/inventory.lua:169
msgid "Search"
msgstr ""

#: mods/creative/inventory.lua:170
msgid "Reset"
msgstr ""

#: mods/creative/inventory.lua:171
msgid "Previous page"
msgstr ""

#: mods/creative/inventory.lua:172
msgid "Next page"
msgstr ""

#: mods/creative/inventory.lua:250
msgid "All"
msgstr ""

#: mods/creative/inventory.lua:251
msgid "Nodes"
msgstr ""

#: mods/creative/inventory.lua:252
msgid "Tools"
msgstr ""

#: mods/creative/inventory.lua:253
msgid "Items"
msgstr ""
