# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Voxelgarden package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Voxelgarden\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/xpanes/init.lua:153
msgid "Glass Pane"
msgstr ""

#: mods/xpanes/init.lua:167
msgid "Steel Bars"
msgstr ""

#: mods/xpanes/init.lua:181
msgid "Wattle"
msgstr ""
