# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Voxelgarden package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Voxelgarden\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/mtg_craftguide/init.lua:19
msgid "Any coal"
msgstr ""

#: mods/mtg_craftguide/init.lua:20
msgid "Any sand"
msgstr ""

#: mods/mtg_craftguide/init.lua:21
msgid "Any wool"
msgstr ""

#: mods/mtg_craftguide/init.lua:22
msgid "Any stick"
msgstr ""

#: mods/mtg_craftguide/init.lua:23
msgid "Any vessel"
msgstr ""

#: mods/mtg_craftguide/init.lua:24
msgid "Any wood planks"
msgstr ""

#: mods/mtg_craftguide/init.lua:25
msgid "Any kind of stone block"
msgstr ""

#: mods/mtg_craftguide/init.lua:26
msgid "Any kind of metal ingot"
msgstr ""

#: mods/mtg_craftguide/init.lua:28
msgid "Any red flower"
msgstr ""

#: mods/mtg_craftguide/init.lua:29
msgid "Any blue flower"
msgstr ""

#: mods/mtg_craftguide/init.lua:30
msgid "Any black flower"
msgstr ""

#: mods/mtg_craftguide/init.lua:31
msgid "Any green flower"
msgstr ""

#: mods/mtg_craftguide/init.lua:32
msgid "Any white flower"
msgstr ""

#: mods/mtg_craftguide/init.lua:33
msgid "Any orange flower"
msgstr ""

#: mods/mtg_craftguide/init.lua:34
msgid "Any violet flower"
msgstr ""

#: mods/mtg_craftguide/init.lua:35
msgid "Any yellow flower"
msgstr ""

#: mods/mtg_craftguide/init.lua:37
msgid "Any red dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:38
msgid "Any blue dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:39
msgid "Any cyan dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:40
msgid "Any grey dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:41
msgid "Any pink dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:42
msgid "Any black dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:43
msgid "Any brown dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:44
msgid "Any green dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:45
msgid "Any white dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:46
msgid "Any orange dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:47
msgid "Any violet dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:48
msgid "Any yellow dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:49
msgid "Any magenta dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:50
msgid "Any dark grey dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:51
msgid "Any dark green dye"
msgstr ""

#: mods/mtg_craftguide/init.lua:183
msgid "G"
msgstr ""

#: mods/mtg_craftguide/init.lua:195
msgid "Any item belonging to the group(s): @1"
msgstr ""

#: mods/mtg_craftguide/init.lua:199
msgid "Unknown Item"
msgstr ""

#: mods/mtg_craftguide/init.lua:200
msgid "Fuel"
msgstr ""

#: mods/mtg_craftguide/init.lua:228
msgid "Usage @1 of @2"
msgstr ""

#: mods/mtg_craftguide/init.lua:229
msgid "Recipe @1 of @2"
msgstr ""

#: mods/mtg_craftguide/init.lua:235
msgid "Previous recipe"
msgstr ""

#: mods/mtg_craftguide/init.lua:236
msgid "Next recipe"
msgstr ""

#: mods/mtg_craftguide/init.lua:242
msgid "Recipe is too big to be displayed."
msgstr ""

#: mods/mtg_craftguide/init.lua:264
msgid "Shapeless"
msgstr ""

#: mods/mtg_craftguide/init.lua:265
msgid "Alloying time: @1"
msgstr ""

#: mods/mtg_craftguide/init.lua:266
msgid "Cooking time: @1"
msgstr ""

#: mods/mtg_craftguide/init.lua:289
msgid "Search"
msgstr ""

#: mods/mtg_craftguide/init.lua:290
msgid "Reset"
msgstr ""

#: mods/mtg_craftguide/init.lua:291
msgid "Previous page"
msgstr ""

#: mods/mtg_craftguide/init.lua:292
msgid "Next page"
msgstr ""

#: mods/mtg_craftguide/init.lua:297
msgid "No items to show."
msgstr ""

#: mods/mtg_craftguide/init.lua:315
msgid "No usages."
msgstr ""

#: mods/mtg_craftguide/init.lua:315
msgid "Click again to show recipes."
msgstr ""

#: mods/mtg_craftguide/init.lua:316
msgid "No recipes."
msgstr ""

#: mods/mtg_craftguide/init.lua:316
msgid "Click again to show usages."
msgstr ""

#: mods/mtg_craftguide/init.lua:437
msgid "Recipes"
msgstr ""
