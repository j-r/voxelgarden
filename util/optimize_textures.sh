#!/bin/bash

# navigate to the game's root dir if ran from util/
if [[ $(basename $PWD) == "util" ]]; then
	cd ..
fi

find -type f -name "*.png" -exec optipng -o7 -nx -strip all {} \;
