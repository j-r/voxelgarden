Contributing to Voxelgarden
===========================

Thank you for your interest in our game!

Voxelgarden is currently maintained by [@rudzik8](https://codeberg.org/rudzik8).
They're the person giving the final approval/disapproval and merging new code.

You can help with Voxelgarden's development in many different ways, regardless
of whether you're a programmer or not.

When contributing or otherwise participating in the project, you should always
follow our [Code Of Conduct (CoC)](CODE_OF_CONDUCT.md). Violating it is a
punishable offense. Let's avoid being jerks here.


What to help with
-----------------

We maintain a list of things that you can take on if you're willing to help. It
is divided into 3 categories: *Code*, *Media* and *Both*, and you're free to
pick an issue (point) from one of them depending on what you're most familiar
with. See [#21 (Contributing hub)](https://codeberg.org/voxelgarden/voxelgarden/issues/21).

You can also report issues yourself! If you encounter something that looks like 
a bug or an oversight, go straight to the [Issue tracker](https://codeberg.org/voxelgarden/voxelgarden/issues)
and, if it hasn’t been reported yet (remember to use the search feature!),
[report a new one](https://codeberg.org/voxelgarden/voxelgarden/issues/new)!
Note that a Codeberg account is required. (Sign up, it's completely free!)

The rest of this document is for those who are contributing code, media assets,
or both.


How exactly to help
-------------------

We, like most other Luanti projects, use [Git](https://git-scm.com/) as our
version control system of choice. If you want to contribute to the project, it
is **highly recommended** that you learn the git basics, even if you aren't
contributing code. [You can easily find some useful guides on it on the web.](https://duckduckgo.com/?q=git+basics+for+beginners)

Every contributor is encouraged to follow a common contributing step-by-step
pipeline to ensure the development process goes smoothly and with little to no
merge conflicts between contributions.

1. **Fork the repo.** You can do this by clicking the "Fork" button on our
   repo page or by accessing [this link](https://codeberg.org/repo/fork/196363).

   _Note for organization members: you already have write access to the repo,
   and thus you can safely ignore this step._

2. **Clone it.** If you're contributing to Voxelgarden, it's likely that you
   already have a copy of it installed somewhere. First, you should locate
   where:

   * If you're using Linux, Mac OS or other unix-like OS, and you installed
     Luanti through your package manager or by compiling it yourself with
     `-DRUN_IN_PLACE=FALSE`, then go to `~/.minetest/games`.
   * If you're using Windows or a similar OS, or you compiled Luanti yourself
     with `-DRUN_IN_PLACE=TRUE`, then go to the directory you have Luanti in
     (should also contain folders like `bin`, `builtin`, `client`, etc)

   Then, on your fork's repo page, copy its link, open up a terminal
   window (or whatever console you have for git operations if you're using
   Windows or a similar OS), and do:

    ```
    git clone <copied link here> voxelgarden-git/
    cd voxelgarden-git/
    ```

   When you open Luanti after completing this step, you'll notice that you now
   have two Voxelgarden icons on the bottom in the Main Menu. When testing your
   changes in step 4, you'll have to pick the Voxelgarden that does not contain
   your worlds, i.e. the one with the odd gameid (`voxelgarden-git`)

3. **Make a new branch.** When you're working on your `master` branch, it looks
   all clean and fine, until someone pushes to the upstream `master`, and now
   you have to force push the new changes and deal with all sorts of weird
   problems. Creating a new branch is important for this very reason.

   Generally you should pick a short yet fairly clear and descriptive name for
   your branch so that it can be easily identified among others. For example, if
   you're contributing a fix for a bug that causes rats to pour from the sky,
   then a good branch name would be `rat_rain_fix`.

    ```
    git checkout master
    git checkout -b <branch name here>
    ```

4. **Commit changes and push them.** This is the step where you make your
   changes and contribute. Don't forget to play-test them along the way. When
   you feel you're done, you can make a commit and push your changes from local
   to remote (i.e. from your machine to the Internet). This is a pretty basic
   thing, but just to be clear:

    ```
    git add .
    git status # confirm that everything is staged for the commit now
    
    git commit -m "Commit name here" -m "Commit description (optional; leave out just one -m parameter if no description is needed)"
    
    git push -u origin <branch name here>
    ```

   After doing this, go to your branch page and confirm that the changes were
   indeed committed and that they are visible.

5. **File a pull request.** Usually Codeberg suggests that automatically when
   visiting your fork page, but it can also be done from [this link](https://codeberg.org/voxelgarden/voxelgarden/compare/master...master).

   Here, you're prompted with two dropdown lists: *merge into* and *pull from*.
   *Merge into* should have `voxelgarden:master` selected, and *pull from*
   should have your branch selected (you may have to scroll down to find it).

   If you picked the right branch, now you should see the commit(-s) you made
   and a big button labeled "New Pull Request". If everything seems correct,
   press it.

   From there you're met with a familiar form, similar to the one you get when
   reporting issues. Here you should give a good description of what changes
   you're committing, which issue these changes close and (if needed) how to
   test them.

If you've done everything right, then congratulations: you just contributed to
Voxelgarden, and are now entitled to a free pet Oerkki. `/jk`

In all seriousness though, thank you for contributing to this project. We all
appreciate it and new people are very welcome here.


-----

This document was partially based on [VoxeLibre's CONTRIBUTING.md](https://git.minetest.land/VoxeLibre/VoxeLibre/src/branch/master/CONTRIBUTING.md).
We highly suggest that you also check out their game if you haven't already!
