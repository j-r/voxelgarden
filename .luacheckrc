unused_args = false
allow_defined_top = true
redefined = false

globals = {
	"default", "voxelgarden",
	"conifer", "vg_alloy",
	"vg_hunger", "vg_formspec",
	"vg_score", "vg_weather",
	"vg_logging",
	"beds",
	"binoculars",
	"bucket", "bs",
	"carts",
	"compostbin",
	"doors", "dye",
	"dungeon_loot", "farming",
	"fire", "flowers",
	"map", "mobs",
	"player_api",
	"screwdriver", "sfinv",
	"stairs", "stairsplus",
	"snowdrift",
	"tnt", "tt", "tt_base",
	"logging", "mesecon",
	"controls", "nether"
}

read_globals = {
	"DIR_DELIM",
	"minetest", "core",
	"dump",
	"vector",
	"VoxelManip", "VoxelArea",
	"PseudoRandom", "PcgRandom",
	"ItemStack",
	"Settings",
	"unpack",
	"hb",
	"cmi", "climate_api",
	"toolranks",
	"invisibility",
	"loot", "doc",
	-- Silence errors about custom table methods
	table = {
		fields = {
			"copy",
			"indexof",
			"insert_all",
			"combine",
			"update",
			"update_nil",
			"pairs_by_keys",
		}
	},
	-- Silence warnings about accessing undefined fields of global 'math'
	math = { fields = { "sign" } }
}

-- Overwrites default definitions for items
files["mods/default/init.lua"].globals = { "minetest" }

-- Overwrites minetest.handle_node_drops
files["mods/creative/init.lua"].globals = { "minetest" }

-- Overwrites falling node
files["mods/physics/falling.lua"].globals = { "minetest", "core" }

-- Overwrites minetest.calculate_knockback
files["mods/player_api/api.lua"].globals = { "minetest" }

-- Overwrites item entity
files["mods/vg_item_entity/init.lua"].globals = { "minetest", "core" }

-- Don't report on legacy definitions of globals.
files["mods/default/legacy.lua"].global = false

-- Custom table fields
files["mods/vg_init/table.lua"].global = false

-- `hunger = vg_hunger`
files["mods/vg_hunger/init.lua"].global = false

-- Old globals and long def tables
files["mods/dye/init.lua"].global = false
files["mods/dye/init.lua"].max_line_length = false

-- Mostly just long descriptions that would break otherwise
files["mods/nether"].max_line_length = false

-- Long comments that are read by i18n tooling and translators, not coders
files["mods/tt_base/localize.lua"].max_line_length = false

exclude_files = {
	-- Codebase graveyards
	-- -------------------
	-- These mods are planned for removal and replacement, so it's not worth
	-- shouting at them.
	"mods/stairsplus"
}
